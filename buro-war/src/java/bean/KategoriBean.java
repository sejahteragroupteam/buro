/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import entity.Kategori;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import org.primefaces.event.RowEditEvent;
import session.KategoriEJBRemote;

/**
 *
 * @author Rangga
 */
@Named(value = "kategoriBean")
@SessionScoped
public class KategoriBean implements Serializable {

    @EJB
    KategoriEJBRemote kategoriEJB;
    List<Kategori> kategoriList = new ArrayList<>();
    Kategori kategori;

    String namaKategori;

    public KategoriBean() {
    }

    public void selectAllBuku() {
        kategoriEJB.findKategoris();
    }

    public KategoriEJBRemote getKategoriEJB() {
        return kategoriEJB;
    }

    public void setKategoriEJB(KategoriEJBRemote kategoriEJB) {
        this.kategoriEJB = kategoriEJB;
    }

    public List<Kategori> getKategoriList() {
        kategoriList = kategoriEJB.findKategoris();
        return kategoriList;
    }

    public void setKategoriList(List<Kategori> kategoriList) {
        this.kategoriList = kategoriList;
    }

    public String getNamaKategori() {
        return namaKategori;
    }

    public void setNamaKategori(String namaKategori) {
        this.namaKategori = namaKategori;
    }

    public Kategori getKategori() {
        return kategori;
    }

    public void setKategori(Kategori kategori) {
        this.kategori = kategori;
    }

    public void addKategori() {
        Kategori k = new Kategori();
        k.setNamaKategori(namaKategori);
        kategoriEJB.createKategori(k);
    }

    public void deleteKategori(Kategori kategori) {
        FacesMessage msg = new FacesMessage("Kategori berhasil dihapus");
        Kategori k = kategoriEJB.findKategoriById(kategori.getIdKategori());
        kategoriEJB.deleteKategori(k);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

//    public void onRowEdit(RowEditEvent event) {
//        FacesMessage msg = new FacesMessage("Kategori berhasil diedit");
//        Kategori k = (Kategori) event.getObject();
//        k.setIdKategori(((Kategori) event.getObject()).getIdKategori());
//        k.setNamaKategori(((Kategori) event.getObject()).getNamaKategori());
//        System.out.println(((Kategori) event.getObject()).getNamaKategori());
//        kategoriEJB.updateKategori(k);
//        FacesContext.getCurrentInstance().addMessage(null, msg);
//    }
//    public void onRowCancel(RowEditEvent event) {
//        FacesMessage msg = new FacesMessage("Edit dicancel");
//        FacesContext.getCurrentInstance().addMessage(null, msg);
//    }
    public void updateKategori() {
        entity.Kategori k = new Kategori();
        k.setIdKategori(kategori.getIdKategori());
        k.setNamaKategori(kategori.getNamaKategori());

        kategoriEJB.updateKategori(k);
    }
}
