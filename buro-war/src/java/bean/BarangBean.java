/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import entity.Barang;
import entity.Kategori;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import session.BarangEJBRemote;
import session.KategoriEJBRemote;

/**
 *
 * @author Rangga
 */
@Named(value = "barangBean")
@SessionScoped
public class BarangBean implements Serializable {
//k
    @EJB
    BarangEJBRemote barangEJB;
    KategoriEJBRemote kategoriEJB;
    Barang barang;
    List<Barang> barangList = new ArrayList<>();

    List<Kategori> kategoriList = new ArrayList<>();
    Kategori selectedKategori;
    double harga;
    String namaBarang;
    int idBarang;
    int stok;
    int idKategori;
    int idKategoriUp;

    public BarangBean() {
        selectedKategori = new Kategori();

    }

    public int getIdBarang() {
        return idBarang;
    }

    public void setIdBarang(int idBarang) {
        this.idBarang = idBarang;
    }
    
    public List<Barang> getBarangList() {
        barangList = barangEJB.findBarangs();
        return barangList;
    }

    public List<Kategori> getKategoriList() {
        kategoriList = kategoriEJB.findKategoris();
        return kategoriList;
    }

    public BarangEJBRemote getBarangEJB() {
        return barangEJB;
    }

    public void setBarangEJB(BarangEJBRemote barangEJB) {
        this.barangEJB = barangEJB;
    }

    public KategoriEJBRemote getKategoriEJB() {
        return kategoriEJB;
    }

    public void setKategoriEJB(KategoriEJBRemote kategoriEJB) {
        this.kategoriEJB = kategoriEJB;
    }

    public Barang getBarang() {
        return barang;
    }

    public void setBarang(Barang barang) {
        this.barang = barang;
    }

    public Kategori getSelectedKategori() {
        return selectedKategori;
    }

    public void setSelectedKategori(Kategori selectedKategori) {
        this.selectedKategori = selectedKategori;
    }

    public double getHarga() {
        return harga;
    }

    public void setHarga(double harga) {
        this.harga = harga;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public int getStok() {
        return stok;
    }

    public void setStok(int stok) {
        this.stok = stok;
    }

    public int getIdKategori() {
        return idKategori;
    }

    public void setIdKategori(int idKategori) {
        this.idKategori = idKategori;
    }

    public int getIdKategoriUp() {
        return idKategoriUp;
    }

    public void setIdKategoriUp(int idKategoriUp) {
        this.idKategoriUp = idKategoriUp;
    }

    public void addBarang() {
        FacesMessage msg = new FacesMessage("Barang berhasil ditambah");

        for (Kategori kxx : kategoriList) {
            if (kxx.getIdKategori().equals(idKategori)) {
                idKategori = kxx.getIdKategori();
                break;
            }
        }

        selectedKategori.setIdKategori(idKategori);
        Barang b = new Barang();
        b.setNamaBarang(namaBarang);
        b.setHarga(harga);
        b.setStok(stok);
        b.setIdKategori(selectedKategori);
        barangEJB.createBarang(b);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public String deleteBarang(Barang barang) {
        FacesMessage msg = new FacesMessage("Barang berhasil dihapus");
        Barang b = barangEJB.findBarangById(barang.getIdBarang());
        barangEJB.deleteBarang(b);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return null;
    }

//    public void onRowEdit(RowEditEvent event) {
//        FacesMessage msg = new FacesMessage("Barang berhasil diedit");
//        Barang b = new Barang();
//        b.setIdBarang(((Barang) event.getObject()).getIdBarang());
//        b.setNamaBarang(((Barang) event.getObject()).getNamaBarang());
//        b.setHarga(((Barang) event.getObject()).getHarga());
//        b.setStok(((Barang) event.getObject()).getStok());
//        barangEJB.updateBarang(b);
//        FacesContext.getCurrentInstance().addMessage(null, msg);
//    }
//
//    public void onRowCancel(RowEditEvent event) {
//        FacesMessage msg = new FacesMessage("Edit dicancel");
//        FacesContext.getCurrentInstance().addMessage(null, msg);
//    }

    public void updateBarang() {
        FacesMessage msg = new FacesMessage("Barang berhasil diupdate");
        for (Kategori kxx : kategoriList) {
            if (kxx.getIdKategori().equals(idKategori)) {
                idKategori = kxx.getIdKategori();
                break;
            }
        }

        selectedKategori.setIdKategori(idKategoriUp);
        Barang b = new Barang();
        b.setIdBarang(barang.getIdBarang());
        b.setNamaBarang(barang.getNamaBarang());
        b.setHarga(barang.getHarga());
        b.setStok(barang.getStok());
        b.setIdKategori(selectedKategori);
        barangEJB.updateBarang(b);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

}
