/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Rangga
 */
@Named(value = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    String username;
    String password;

    public LoginBean() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String logins() {
        boolean login = false;
        if (username.equalsIgnoreCase("admin") && password.equals("admin")) {
            login = true;
        } else {
            login = false;
            FacesMessage mess = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Username / Password Salah", "");
            FacesContext.getCurrentInstance().addMessage(null, mess);
        }
        if (login) {
            return "admin/menuAwal.xhtml?faces-redirect=true";
        }
        
        return null;
    }
}
