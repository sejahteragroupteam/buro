/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Barang;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author abi_v
 */
@Stateless
public class BarangEJB implements BarangEJBRemote  {

    @PersistenceContext(unitName = "buro-ejbPU")
    private EntityManager em;
    
    @Override
    public List<Barang> findBarangs(){
        Query query = em.createNamedQuery("findAllBarang");
        return query.getResultList();
    }
    @Override
    public Barang findBarangById(Integer id){
        return em.find(Barang.class, id);
    }
    
    @Override
    public Barang createBarang(Barang barang){
        em.persist(barang);
        return barang;
    }
    
    @Override
    public void deleteBarang(Barang barang){
        em.remove(em.merge(barang));
    }
    
    @Override
    public Barang updateBarang(Barang barang){
        return em.merge(barang);
    }
}
