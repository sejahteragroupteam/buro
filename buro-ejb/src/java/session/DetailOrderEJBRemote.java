/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Barang;
import entity.Customer;
import entity.DetailOrder;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author abi_v
 */
@Remote
public interface DetailOrderEJBRemote {

    public void addItem(Barang item);
    

    public void removeItem(DetailOrder item);

    public Double getTotal();

    public void checkout(Customer c);

    public void empty();

    public List<Barang> getCartItems();

    public void setCartItems(List<Barang> cartItems);

    public List<DetailOrder> getListDo();

    public void setListDo(List<DetailOrder> listDo);
    
}
