/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Customer;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author abi_v
 */
@Stateless
public class CustomerEJB implements CustomerEJBRemote{

    @PersistenceContext(unitName = "buro-ejbPU")
    private EntityManager em;
    
    @Override
    public List<Customer> findCustomers(){
        Query query = em.createNamedQuery("findAllCustomer");
        return query.getResultList();
    }
    @Override
    public Customer findCustomerById(Integer id){
        return em.find(Customer.class, id);
    }
    
    @Override
    public Customer createCustomer(Customer customer){
        em.persist(customer);
        return customer;
    }
    
    @Override
    public void deleteCustomer(Customer customer){
        em.remove(em.merge(customer));
    }
    
    @Override
    public Customer updateCustomer(Customer customer){
        return em.merge(customer);
    }
}
